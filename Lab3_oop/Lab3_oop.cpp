#include <iostream>
#include <locale>
#include <vector>
using namespace std;

//�������� ��������
const int V = 6;
void Dijkstra(int GR[V][V], int st)
{
	int distance[V], count, index, i, u, m = st + 1;
	bool visited[V];
	for (i = 0; i < V; i++)
	{
		distance[i] = INT_MAX; visited[i] = false;
	}
	distance[st] = 0;
	for (count = 0; count < V - 1; count++)
	{
		int min = INT_MAX;
		for (i = 0; i < V; i++)
			if (!visited[i] && distance[i] <= min)
			{
				min = distance[i]; index = i;
			}
		u = index;
		visited[u] = true;
		for (i = 0; i < V; i++)
			if (!visited[i] && GR[u][i] && distance[u] != INT_MAX &&
				distance[u] + GR[u][i] < distance[i])
				distance[i] = distance[u] + GR[u][i];
	}
	cout << "��������� ���� �� ��������� ������� �� ���������:\t\n";
	for (i = 0; i < V; i++) if (distance[i] != INT_MAX)
		cout << m << " > " << i + 1 << " = " << distance[i] << endl;
	else cout << m << " > " << i + 1 << " = " << "������� ����������" << endl;
}

//int Minvalue1(int *vertex)
//{
//	int minvalue = INT_MAX;
//	for (int i = 0; i < V; i++)
//	{
//		if (vertex[i] != 0 && vertex[i] < minvalue)
//		{
//			minvalue = vertex[i];
//		}
//	}
//	return minvalue;
//}
//
//int minIndex(int *vertex)
//{
//	int minindex = 0;
//	for (int i = 0; i < V; i++)
//	{
//		if (vertex[i] < vertex[minindex] && vertex[i] >= 1)
//		{
//			minindex = i;
//		}
//	}
//	return minindex;
//}

int Sum_of_edges(int arr[V][V])
{
	int sum_of_edges = 0;
	for (int i = 0; i < V; i++)
	{
		for (int j = 0; j < V; j++)
		{
			if (arr[i][j] != 0)
				sum_of_edges++;
		}
	}
	return sum_of_edges / 2;
}

//�������� �����
void Prima(int GR[V][V])
{
	const int sum = Sum_of_edges(GR); //������� ���������� ����� �����
	vector<int> visited; //����� ������, ������� �� �������� � ������

	/*for (int i = 0; i < V; i++) //����������� �� ��������� � ���� ���������, ������� ����������� ������
	{
		for (int j = 0; j < V; j++)
		{
			if (j <= i || GR[i][j] == 0)
			{
				GR[i][j] = INT_MAX;
			}
		}
	}*/

	int min = INT_MAX, sourceMin = -1, targetMin = -1;
	visited.push_back(0);

	for (int ii = 0; ii < visited.size(); ii++) // ���� ������� ����������� �� ��������� �������
	{
		for (int i = 0; i < visited.size(); i++)
		{
			int vnum = visited[i]; //����� �� ���������� ������
			for (int j = vnum + 1; j < V; j++)
			{
				if (GR[vnum][j] != 0)
					if (GR[vnum][j] < min) //�������� ������� 
					{
						min = GR[vnum][j];
						targetMin = j;
						//
					}
			}
			GR[vnum][targetMin] = 0;
			if (visited.back() != targetMin)
				visited.push_back(targetMin);
		}
		min = INT_MAX;
	}

	for (int i = 0; i < visited.size(); i++)
	{
		cout << visited[i] << " ";
	}
}
//prima inet
//void PrimaInt(int cost[V][V])
//{
//	int a, b, u, v, i, j, ne = 1;
//	int visited[10] = { 0 }, min, mincost = 0;
//
//	int path[100] = { 0 }; //� ���� ������ ����� ������������ �������, �� ������� ����������� ����
//	int path_index = 0;
//
//	for (i = 1; i <= V; i++)
//		for (j = 1; j <= V; j++)
//		{
//			if (cost[i][j] == 0)
//				cost[i][j] = 999; //999 - ��� ���-���� �������������. ������ ���� ������ ��� �������� ���� ������� �� ����� � �����
//		}
//	visited[1] = 1;
//
//	while (ne < V)
//	{
//		for (i = 1, min = 999; i <= V; i++)
//			for (j = 1; j <= V; j++)
//				if (cost[i][j] < min)
//					if (visited[i] != 0)
//					{
//						min = cost[i][j];
//						a = u = i;
//						b = v = j;
//					}
//		if (visited[u] == 0 || visited[v] == 0)
//		{
//			path[path_index] = b;
//			path_index++;
//			//cout<<"\n "<<ne++<<"  "<<a<<"  "<<b<<min; //����� ������� ���
//			ne++; //���� ������� ���� ����������������� - ��� ����������������
//			mincost += min;
//			visited[b] = 1;
//
//		}
//		cost[a][b] = cost[b][a] = 999;
//	}
//
//	cout << 1 << " --> ";
//	for (int i = 0; i < V - 1; i++)
//	{
//		cout << path[i];
//		if (i < V - 2) cout << " --> ";
//	}
//
//	cout << "\n ����������� ���������  " << mincost;
//	cin.get();
//	cin.get();
//}

void Prima2()
{
	// ������� ������
	int n = 6; // ����� ������ �����
	vector < vector<int> > g = {
	{0, 4, 1, 0, 2, 0},
	{4, 0, 0, 9, 0, 0},
	{1, 0, 0, 7, 0, 0},
	{0, 9, 7, 0, 0, 2},
	{2, 0, 0, 0, 0, 8},
	{0, 0, 0, 2, 8, 0} };;
	const int INF = 1000000000; // �������� "�������������"

	// ��������
	vector<bool> used(n);
	vector<int> min_e(n, INF), sel_e(n, -1);
	min_e[0] = 0;
	for (int i = 0; i < n; ++i) 
	{
		int v = -1;
		for (int j = 0; j < n; ++j)
			if (!used[j] && (v == -1 || min_e[j] < min_e[v]))
				v = j;
		if (min_e[v] == INF) {
			cout << "No MST!";
			exit(0);
		}

		used[v] = true;
		if (sel_e[v] != -1)
			cout << v << " " << sel_e[v] << endl;

		for (int to = 0; to < n; ++to)
			if (g[v][to] < min_e[to]) {
				min_e[to] = g[v][to];
				sel_e[to] = v;
			}
	}
}


const int v = 5;
//�������� �����-����������
void FordFulkerson(int GR[v][v])
{
	
	vector<int> S;
	int k = 1;
	while(k != 5)
	{ 
		// step 1-2
		for (int i = 0; i < v; i++)
		{
			if (GR[k-1][i] != 0)
			{
				S.push_back(GR[0][i]);
			}
		}

		if (!S.empty())
		{
			// step 3
			int max = INT_MIN;
			for (int i = 0; i < v; i++)
			{
				if (GR[k - 1][i] != 0 && GR[k-1][i] > max )
				{
					max = GR[k-1][i];
					cout << max << endl;
					cout << i << endl;
					
				}
				
				 //��� ��� �� ������������� ������ ������� (0-��� ������ �������)
			}
			k = i;
			//
			//vector <int> a = {max};
			//vector<vector<int>> mark = { {INT_MAX, INT_MAX}, {max, k} }; // ���� (ak, i), ��� �� - ��������� ������� � ����� �������, i - ���������� �������
			
		//	
		//	// step 5
		//	vector <int> N;
		//	cout << "N = {";
		//	for (int i = 0; i < mark.size(); i++)
		//	{
		//		N[i] = mark[i][0]; //������������������� N ������-� ���������� ��������� ����
		//		cout << " "  << N[i] << " ";
		//	}
		//	cout << "}" << endl;
		//	int fmin = INT_MAX; // ���������� ����������� ��������� ����
		//	for (int i = 0; i < N.size(); i++)
		//	{
		//		if (N[i] < fmin)
		//		{
		//			fmin = N[i];
		//		}
		//	}
		//	cout << "fmin = " << fmin << endl;
		////	for (int i = 0; i < v; i++)
		////	{
		////		for (int j = 0; j < v; j++)
		////		{
		////			//GR[i][j]
		////		}
		////	}
		}
		else cout << "no edges frov this vertex" << endl;

	}
}


int main()
{
	setlocale(LC_ALL, "RU");
	/*
	/*int start, GR[V][V] = {
	{0, 1, 4, 0, 2, 0},
	{0, 0, 0, 9, 0, 0},
	{0, 0, 0, 7, 0, 0},
	{0, 0, 0, 0, 0, 2},
	{0, 0, 0, 0, 0, 8},
	{0, 0, 0, 0, 0, 0} };*/

	/*
		vector<int> verticesInSkel;
		step 1
		{0}
		step 2
		{0, 2}
		step 3
		{0, 2, 5}

		int min = 10000, sourceMin = -1, targetMin = -1;
		for(int i=0;i<verticesInSkel.size();i++) 
		{
			vnum = verticesInSkel[i];
			for(int j=0;j<V;j++) 
			{
				if(min < GR[vnum][j]) 
				{
					min = GR[vnum][j];
					sourceMin = i;
					targetMin = j;
				}
			}
		}
		//������� �������� �� ��������� j � 

	*/

	/*
		visited[0] = minIndex(newGR[0]);
			for(int i=0;i<newGR.size();i++) 
		{
			int vnum = verticesInSkel[i];
			for(int j=0;j<V;j++) 
			{

			}
		}
	*/

	/*int start, GR[V][V] = {
	{0, 4, 1, 0, 2, 0},
	{4, 0, 0, 9, 0, 0},
	{1, 0, 0, 7, 0, 0},
	{0, 9, 7, 0, 0, 2},
	{2, 0, 0, 0, 0, 8},
	{0, 0, 0, 2, 8, 0}    };
	//1
	cout << "��������� ������� >> "; cin >> start;
	Dijkstra(GR, start - 1);
	//2
	Prima(GR);*/
	
	int GR[v][v] =
	{
		{0, 20, 30, 10, 0},
		{0, 0, 40, 0, 30},
		{0, 0, 0, 10, 20},
		{0, 0, 0, 0, 20},
		{0, 0, 0, 0, 0},
	};

	FordFulkerson(GR);
	return 0;
}
